package com.example.itschool.registerationexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher{

    Button bRegister;
    EditText etLogin, etPassword;
    TextView etPassLength, etloginLength, etPasscomplexity;
    ImageView OK1, OK2, OK3;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bRegister = (Button) findViewById(R.id.b_register);
        etLogin = (EditText) findViewById(R.id.et_login);
        etPassword = (EditText) findViewById(R.id.et_password);
        etPassLength = (TextView) findViewById(R.id.etPassLength);
        bRegister.setOnClickListener(this);
        etPassword.addTextChangedListener(this);
        etloginLength =(TextView) findViewById(R.id.etLogLength);
        etLogin.addTextChangedListener(this);
        etPasscomplexity = (TextView) findViewById(R.id.etPasscomplexity);
        OK1 = (ImageView) findViewById(R.id.ImageOk1);
        OK2 = (ImageView) findViewById(R.id.ImageOk2);
        OK3 = (ImageView) findViewById(R.id.ImageOk3);
    }



    @Override
    public void onClick(View view) {
        String etPassword1 = etPassword.getText().toString();
        String etLogin1 = etLogin.getText().toString();
        if (Validator.PasswordCheck(etPassword1) &&  Validator.LoginCheck(etLogin1) && Validator.PassLengthCheck(etPassword1)){
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("login", etLogin.getText().toString());
            intent.putExtra("password", etPassword.getText().toString());
            startActivity(intent);
        }
        }



    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }
    @Override
    public void afterTextChanged(Editable s ) {

        String etPassword1 = etPassword.getText().toString();
        String etLogin1 = etLogin.getText().toString();
        if (Validator.LoginCheck(etLogin1)) {
            etloginLength.setText("Введите логин ");
            OK1.setImageResource(R.drawable.success);
        }
        else {
            etloginLength.setText("Введите логин");
        }

        if (Validator.PassLengthCheck(etPassword1)) {
            etPassLength.setText ("Ваш пароль достаточной длины");
            OK2.setImageResource(R.drawable.success);
        }
        else {
            etPassLength.setText ("Минимальная длина пароля: 7 символов");
        }

        if (Validator.PasswordCheck(etPassword1)) {
            etPasscomplexity.setText ("Ваш пароль достаточно сложный");
            OK3.setImageResource(R.drawable.success);
        }
        else {
            etPasscomplexity.setText ("Ваш пароль слишком легкий");
        }





    }
}
